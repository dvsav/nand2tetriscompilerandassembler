// This file is a part of project 09 of the course
// "Build a Modern Computer from First Principles: Nand to Tetris Part II"
// https://www.coursera.org/learn/nand2tetris2/

/** Implements the [StartMenu]. */
class StartMenu
{
    field Rectangle border;  // the border for the whole [StartMenu]
    field boolean isVisible; // visibility of the [StartMenu]

    /** Creates the [StartMenu] */
    constructor StartMenu new()
    {
        let border = Rectangle.new(0, 173, 112, 235);
        let isVisible = false;
        return this;
    }
    
    /** Disposes this [StartMenu] */
    method void dispose()
    {
        do Memory.deAlloc(this);
        return;
    }
    
    /** Sets the visibility of the [StartMenu] */
    method void setIsVisible(boolean value)
    {
        let isVisible = value;
        return;
    }
    
    /** Gets the visibility of the [StartMenu] */
    method boolean getIsVisible()
    {
        return isVisible;
    }
  
    /** Draws the [StartMenu] on the screen */
    method void draw()
    {
        if(isVisible)
        {
            // Tetris
            do Sprites.drawTetris(0, 11);
            do Output.moveCursor(16, 3);
            do Output.printString("Tetris");
            
            do Screen.drawLine(0, 193, 107, 193); // separator line
            
            // Notepad
            do Sprites.drawNotepad(128, 12);
            do Output.moveCursor(18, 3);
            do Output.printString("Notepad");
            
            do Screen.drawLine(0, 215, 107, 215); // separator line
           
            // Exit
            do Sprites.drawStandBy(256, 13);
            do Output.moveCursor(20, 3);
            do Output.printString("Exit");
            
            do border.draw();
        }
        
        return;
    }
    
    /** Erases the [StartMenu] from the screen */
    method void erase()
    {
        do border.erase();
        return;
    }
    
    /**
     * Returns:
     * True  - pos is inside the [StartMenu]
     * False - pos is outside the [StartMenu]
     */
    method boolean hitTest(Point pos)
    {
        return isVisible & (pos.getY() < 15) &  (pos.getY() > 9) &  (pos.getX() < 8);
    }
    
    /**
     * Returns:
     * True  - pos is over the [Exit] button
     * False - pos is away from the [Exit] button
     */
    method boolean hitTestExit(Point pos)
    {
        return isVisible & (pos.getY() = 14) &  (pos.getX() < 8);
    }
}
