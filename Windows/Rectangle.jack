// This file is a part of project 09 of the course
// "Build a Modern Computer from First Principles: Nand to Tetris Part II"
// https://www.coursera.org/learn/nand2tetris2/

/** Implements a [Rectangle] geometric shape. */
class Rectangle
{
    field int x1, y1; // top-left corner (coordinates are in pixels)
    field int x2, y2; // bottom-right corner (coordinates are in pixels)

    /**
     * Creates a new [Rectangle] with specified
     * top-left and-bottom right corners.
     * Coordinates are in pixels.
     */
    constructor Rectangle new(int ax1, int ay1, int ax2, int ay2)
    {
        let x1 = ax1;
        let x2 = ax2;
        let y1 = ay1;
        let y2 = ay2;
        return this;
    }
    
   /** Disposes this [Rectangle]. */
    method void dispose()
    {
        do Memory.deAlloc(this);
        return;
    }

    /** Gets the X coordinate of the top-left corner of the [Rectangle]. */
    method int getX1() {return x1;}
    
    /** Gets the Y coordinate of the top-left corner of the [Rectangle]. */
    method int getY1() {return y1;}
    
    /** Gets the X coordinate of the bottom-right corner of the [Rectangle]. */
    method int getX2() {return x2;}
    
    /** Gets the Y coordinate of the bottom-right corner of the [Rectangle]. */
    method int getY2() {return y2;}
    
    /** Draws the [Rectangle] on the screen. */
    method void draw()
    {
        do Graphics.drawRectangle(x1, y1, x2, y2);
        return;
    }
    
    /** Erases the [Rectangle] from the screen. */
    method void erase()
    {
        do Screen.setColor(false);
        do Screen.drawRectangle(x1, y1, x2, y2);
        do Screen.setColor(true);
        return;
    }
    
    /**
     * Returns:
     * True  - pos is inside the [Rectangle]
     * False - pos is outside the [Rectangle]
     * Coordinates are in pixels.
     */
    method boolean hitTest(int x, int y)
    {
        return ((x+1 > x1) & (x-1 < x2) & (y+1 > y1) & (y-1 < y2));
    }
}
