#pragma once

#include <vector>
#include <stdexcept>
#include <algorithm>

#include "Token.h"
#include "TreeElement.h"
#include "CodeGenerator.h"

class Parser
{
private:
    CodeGenerator& codeGenerator;
    std::vector<Token*>::iterator currentToken;
    std::vector<Token*>::iterator begin;
    std::vector<Token*>::iterator end;

public:
    Parser(
        CodeGenerator& codeGenerator,
        std::vector<Token*>::iterator begin,
        std::vector<Token*>::iterator end) :

        codeGenerator(codeGenerator),
        begin(begin),
        end(end)
    { }

    TreeElement* Parse()
    {
        currentToken = begin;
        return Class();
    }

private:
    void AdvanceToken()
    {
        if (currentToken == end)
            throw new std::runtime_error("Unexpected end of token stream");

        currentToken++;
    }

    // ================= LEXICAL ELEMENTS =================

    TreeElement* Keyword(KeywordTypeEnum keywordType)
    {
        if ((*currentToken)->getTokenType() == TokenTypeEnum::KEYWORD)
        {
            auto kwd = static_cast<KeywordToken*>(*currentToken);

            if (kwd->getKeywordType() == keywordType)
            {
                auto token = *currentToken;
                AdvanceToken();
                return new TreeElement(token);
            }
        }

        return nullptr;
    }

    TreeElement* Identifier()
    {
        if ((*currentToken)->getTokenType() == TokenTypeEnum::IDENTIFIER)
        {
            auto token = *currentToken;
            AdvanceToken();
            return new TreeElement(token);
        }
        else
            return nullptr;
    }

    TreeElement* Symbol(char symbol)
    {
        if ((*currentToken)->getTokenType() != TokenTypeEnum::SYMBOL)
            return nullptr;

        SymbolToken* st = static_cast<SymbolToken*>(*currentToken);
        if (st->getSymbol() != symbol)
            return nullptr;

        auto token = *currentToken;
        AdvanceToken();
        return new TreeElement(token);
    }

    TreeElement* IntegerConstant()
    {
        if ((*currentToken)->getTokenType() == TokenTypeEnum::INTEGER_CONST)
        {
            auto token = *currentToken;
            AdvanceToken();
            codeGenerator.PushConst(static_cast<const IntegerConstToken*>(token)->getIntVal());
            return new TreeElement(token);
        }
        else
            return nullptr;
    }

    TreeElement* StringConstant()
    {
        if ((*currentToken)->getTokenType() == TokenTypeEnum::STRING_CONST)
        {
            auto token = *currentToken;
            AdvanceToken();
            codeGenerator.PushStringConst(static_cast<const StringConstToken*>(token)->getStringVal());
            return new TreeElement(token);
        }
        else
            return nullptr;
    }

    TreeElement* KeywordConstant()
    {
        if ((*currentToken)->getTokenType() == TokenTypeEnum::KEYWORD)
        {
            KeywordToken* kwd = static_cast<KeywordToken*>(*currentToken);

            switch (kwd->getKeywordType())
            {
            case KeywordTypeEnum::TRUE:
            case KeywordTypeEnum::FALSE:
            case KeywordTypeEnum::KWD_NULL:
            case KeywordTypeEnum::THIS:
                codeGenerator.PushKeywordConst(kwd);
                auto token = *currentToken;
                AdvanceToken();
                return new TreeElement(token);
            }
        }
        return nullptr;
    }

    TreeElement* UnaryOp()
    {
        auto symbol = Symbol('-');
        if (!symbol) symbol = Symbol('~');
        return symbol;
    }

    // ================= PROGRAM STRUCTURE =================

    TreeElement* Class()
    {
        TreeElement* _class = new TreeElement("class");

        // class
        _class->AddChild(Keyword(KeywordTypeEnum::CLASS));

        // ClassName
        auto class_name = _class->AddChild(Identifier());

        codeGenerator.Class(
            static_cast<const IdentifierToken*>(class_name->getValue()));

        // {
        _class->AddChild(Symbol('{'));

        while (true)
        {
            // }
            if (auto close_brace = Symbol('}'))
            {
                _class->AddChild(close_brace);
                break;
            }

            // class variable declaration
            // subroutine declaration
            if(auto class_var_dec = ClassVarDec())
                _class->AddChild(class_var_dec);
            else if(auto subroutine_dec = SubroutineDec())
                _class->AddChild(subroutine_dec);
            else
                throw std::runtime_error("Class");
        }

        return _class;
    }

    TreeElement* ClassVarDec()
    {
        auto field_specifier = Keyword(KeywordTypeEnum::STATIC);
        if (!field_specifier) field_specifier = Keyword(KeywordTypeEnum::FIELD);
        if (field_specifier)
        {
            TreeElement* classVarDec = new TreeElement("classVarDec");

            // static | field
            classVarDec->AddChild(field_specifier);

            // field type
            auto field_type = classVarDec->AddChild(VarType());

            // field name
            auto field_name = classVarDec->AddChild(Identifier());

            codeGenerator.AddClassVariable(
                /*kind*/ static_cast<const KeywordToken*>(field_specifier->getValue()),
                /*type*/ field_type->getValue(),
                /*name*/ static_cast<const IdentifierToken*>(field_name->getValue())
            );

            while (true)
            {
                // ;
                if (auto semicolon = Symbol(';'))
                {
                    classVarDec->AddChild(semicolon);
                    break;
                }

                // ,
                classVarDec->AddChild(Symbol(','));

                // variable name
                field_name = classVarDec->AddChild(Identifier());

                codeGenerator.AddClassVariable(
                    /*kind*/ static_cast<const KeywordToken*>(field_specifier->getValue()),
                    /*type*/ field_type->getValue(),
                    /*name*/ static_cast<const IdentifierToken*>(field_name->getValue())
                );
            }

            return classVarDec;
        }
        else
            return nullptr;
    }

    TreeElement* SubroutineDec()
    {
        auto subroutine_specifier = Keyword(KeywordTypeEnum::CONSTRUCTOR);
        if (!subroutine_specifier) subroutine_specifier = Keyword(KeywordTypeEnum::FUNCTION);
        if (!subroutine_specifier) subroutine_specifier = Keyword(KeywordTypeEnum::METHOD);
        if (subroutine_specifier)
        {
            auto subroutineDec = new TreeElement("subroutineDec");

            // constructor | function | method
            subroutineDec->AddChild(subroutine_specifier);

            // subroutine return type
            subroutineDec->AddChild(ReturnType());

            // subroutine name
            auto name = subroutineDec->AddChild(Identifier());

            codeGenerator.BeginSubroutineDeclaration(
                /*kind*/ static_cast<const KeywordToken*>(subroutine_specifier->getValue()),
                /*name*/ static_cast<const IdentifierToken*>(name->getValue()));

            // (
            subroutineDec->AddChild(Symbol('('));

            // subroutine parameter list
            subroutineDec->AddChild(ParameterList());

            // )
            subroutineDec->AddChild(Symbol(')'));

            // subroutine body
            subroutineDec->AddChild(SubroutineBody());

            return subroutineDec;
        }
        else
            return nullptr;
    }

    TreeElement* ParameterList()
    {
        auto parameterList = new TreeElement("parameterList");

        if (auto var_type = VarType())
        {
            // parameter type
            auto type = parameterList->AddChild(var_type);

            // parameter name
            auto name = parameterList->AddChild(Identifier());

            codeGenerator.AddSubroutineParameter(
                /*type*/ type->getValue(),
                /*name*/ static_cast<const IdentifierToken*>(name->getValue())
            );

            while (auto comma = Symbol(','))
            {
                // ,
                parameterList->AddChild(comma);

                // parameter type
                type = parameterList->AddChild(VarType());

                // parameter name
                name = parameterList->AddChild(Identifier());

                codeGenerator.AddSubroutineParameter(
                    /*type*/ type->getValue(),
                    /*name*/ static_cast<const IdentifierToken*>(name->getValue())
                );
            }
        }

        return parameterList;
    }

    TreeElement* SubroutineBody()
    {
        if (auto open_brace =  Symbol('{'))
        {
            auto subroutineBody = new TreeElement("subroutineBody");

            // {
            subroutineBody->AddChild(open_brace);

            // variable declaration
            while (auto var_dec = VarDec())
                subroutineBody->AddChild(var_dec);

            codeGenerator.EndSubroutineDeclaration();

            // statements
            if (auto statements = Statements())
                subroutineBody->AddChild(statements);

            // }
            subroutineBody->AddChild(Symbol('}'));

            return subroutineBody;
        }
        else
            return nullptr;
    }

    TreeElement* VarDec()
    {
        if (auto var = Keyword(KeywordTypeEnum::VAR))
        {
            auto varDec = new TreeElement("varDec");

            // var
            varDec->AddChild(var);

            // variable type
            auto type = varDec->AddChild(VarType());

            // variable name
            auto name = varDec->AddChild(Identifier());

            codeGenerator.AddSubroutineLocalVariable(
                /*type*/ type->getValue(),
                /*name*/ static_cast<const IdentifierToken*>(name->getValue())
            );

            while (true)
            {
                // ;
                if (auto semicolon = Symbol(';'))
                {
                    varDec->AddChild(semicolon);
                    break;
                }

                // ,
                varDec->AddChild(Symbol(','));

                // variable name
                name = varDec->AddChild(Identifier());

                codeGenerator.AddSubroutineLocalVariable(
                    /*type*/ type->getValue(),
                    /*name*/ static_cast<const IdentifierToken*>(name->getValue())
                );
            }

            return varDec;
        }
        else
            return nullptr;
    }

    TreeElement* VarType()
    {
        auto kwd = Keyword(KeywordTypeEnum::INT);
        if (!kwd) kwd = Keyword(KeywordTypeEnum::CHAR);
        if (!kwd) kwd = Keyword(KeywordTypeEnum::BOOLEAN);
        if (!kwd) kwd = Identifier();

        return kwd;
    }

    TreeElement* ReturnType()
    {
        auto kwd = Keyword(KeywordTypeEnum::VOID);
        if (!kwd) kwd = Keyword(KeywordTypeEnum::INT);
        if (!kwd) kwd = Keyword(KeywordTypeEnum::CHAR);
        if (!kwd) kwd = Keyword(KeywordTypeEnum::BOOLEAN);
        if (!kwd) kwd = Identifier();

        return kwd;
    }

    // ================= STATEMENTS =================

    TreeElement* Statements()
    {
        auto statements = new TreeElement("statements");

        while (auto statement = Statement())
            statements->AddChild(statement);

        return statements;
    }

    TreeElement* Statement()
    {
        auto statement = LetStatement();
        if (!statement) statement = IfStatement();
        if (!statement) statement = WhileStatement();
        if (!statement) statement = DoStatement();
        if (!statement) statement = ReturnStatement();
        
        return statement;
    }

    TreeElement* LetStatement()
    {
        if (auto let = Keyword(KeywordTypeEnum::LET))
        {
            auto letStatement = new TreeElement("letStatement");

            // let
            letStatement->AddChild(let);

            // variable name
            auto var_name = letStatement->AddChild(Identifier());

            bool is_array = false;
            // [expression]
            if (auto square_brace_open = Symbol('['))
            {
                is_array = true;

                // push array
                codeGenerator.PushVar(static_cast<const IdentifierToken*>(var_name->getValue()));

                // [
                letStatement->AddChild(square_brace_open);

                // expression
                letStatement->AddChild(Expression());

                // ]
                letStatement->AddChild(Symbol(']'));

                // add
                codeGenerator.Add();
            }

            // =
            letStatement->AddChild(Symbol('='));

            // expression
            letStatement->AddChild(Expression());

            // ;
            letStatement->AddChild(Symbol(';'));

            if (is_array)
            {
                // pop temp 0
                codeGenerator.PopTemp0();

                // pop pointer 1
                codeGenerator.PopPointer1();

                // push temp 0
                codeGenerator.PushTemp0();

                // pop that 0
                codeGenerator.PopThat0();
            }
            else
                codeGenerator.PopVar(static_cast<const IdentifierToken*>(var_name->getValue()));

            return letStatement;
        }
        else
            return nullptr;
    }

    TreeElement* IfStatement()
    {
        if (auto _if = Keyword(KeywordTypeEnum::IF))
        {
            auto label_index = std::to_string(codeGenerator.EmitLabelIndex());

            auto ifStatement = new TreeElement("ifStatement");

            // if
            ifStatement->AddChild(_if);

            // (
            ifStatement->AddChild(Symbol('('));

            // expression
            ifStatement->AddChild(Expression());

            // )
            ifStatement->AddChild(Symbol(')'));

            // not
            codeGenerator.Not();
            // if-goto IF_FALSE_i
            codeGenerator.IfGoTo("IF_FALSE_" + label_index);

            // {
            ifStatement->AddChild(Symbol('{'));

            // statements if TRUE
            ifStatement->AddChild(Statements());

            // }
            ifStatement->AddChild(Symbol('}'));

            // goto END_IF_i
            codeGenerator.GoTo("END_IF_" + label_index);
            // label IF_FALSE_i
            codeGenerator.Label("IF_FALSE_" + label_index);

            if (auto _else = Keyword(KeywordTypeEnum::ELSE))
            {
                // else
                ifStatement->AddChild(_else);

                // {
                ifStatement->AddChild(Symbol('{'));

                // statements if FALSE
                ifStatement->AddChild(Statements());

                // }
                ifStatement->AddChild(Symbol('}'));
            }

            // label END_IF_i
            codeGenerator.Label("END_IF_" + label_index);

            return ifStatement;
        }
        else
            return nullptr;
    }

    TreeElement* WhileStatement()
    {
        if (auto _while = Keyword(KeywordTypeEnum::WHILE))
        {
            auto label_index = std::to_string(codeGenerator.EmitLabelIndex());

            // label WHILE_i
            codeGenerator.Label("WHILE_" + label_index);

            auto whileStatement = new TreeElement("whileStatement");

            // while
            whileStatement->AddChild(_while);

            // (
            whileStatement->AddChild(Symbol('('));

            // expression
            whileStatement->AddChild(Expression());

            // )
            whileStatement->AddChild(Symbol(')'));

            // not
            codeGenerator.Not();
            // if-goto END_WHILE_i
            codeGenerator.IfGoTo("END_WHILE_" + label_index);

            // {
            whileStatement->AddChild(Symbol('{'));

            // statements
            whileStatement->AddChild(Statements());

            // }
            whileStatement->AddChild(Symbol('}'));

            // goto WHILE_i
            codeGenerator.GoTo("WHILE_" + label_index);
            // label END_WHILE_i
            codeGenerator.Label("END_WHILE_" + label_index);

            return whileStatement;
        }
        else
            return nullptr;
    }

    TreeElement* DoStatement()
    {
        if (auto _do = Keyword(KeywordTypeEnum::DO))
        {
            auto doStatement = new TreeElement("doStatement");

            // do
            doStatement->AddChild(_do);

            // subroutine call
            if (!SubroutineCall(doStatement))
                throw std::runtime_error("doStatement");

            // ;
            doStatement->AddChild(Symbol(';'));

            // pop void return value
            codeGenerator.PopTemp0();

            return doStatement;
        }
        else
            return nullptr;
    }

    TreeElement* ReturnStatement()
    {
        if (auto _return = Keyword(KeywordTypeEnum::RETURN))
        {
            auto returnStatement = new TreeElement("returnStatement");

            // return
            returnStatement->AddChild(_return);

            // expression?
            if (auto expr = Expression())
                returnStatement->AddChild(expr);
            else
                codeGenerator.PushConst(0);

            // ;
            returnStatement->AddChild(Symbol(';'));

            codeGenerator.Return();

            return returnStatement;
        }
        else
            return nullptr;
    }

    // ================= EXPRESSIONS =================

    TreeElement* Expression()
    {
        if (auto term = Term())
        {
            auto expression = new TreeElement("expression");

            // term
            expression->AddChild(term);

            while (auto op = Op())
            {
                // +-*/&|<>=
                expression->AddChild(op);

                // term
                expression->AddChild(Term());

                codeGenerator.BinaryOperator(
                    static_cast<const SymbolToken*>(op->getValue()));
            }

            return expression;
        }
        else
            return nullptr;
    }

    TreeElement* Op()
    {
        auto op = Symbol('+');
        if (!op) op = Symbol('-');
        if (!op) op = Symbol('*');
        if (!op) op = Symbol('/');
        if (!op) op = Symbol('&');
        if (!op) op = Symbol('|');
        if (!op) op = Symbol('<');
        if (!op) op = Symbol('>');
        if (!op) op = Symbol('=');

        return op;
    }

    TreeElement* Term()
    {
        // int constant
        if (auto int_const = IntegerConstant())
        {
            auto term = new TreeElement("term");
            term->AddChild(int_const);
            return term;
        }

        // string constant
        if (auto string_const = StringConstant())
        {
            auto term = new TreeElement("term");
            term->AddChild(string_const);
            return term;
        }

        // keyword constant
        if (auto kwd_const = KeywordConstant())
        {
            auto term = new TreeElement("term");
            term->AddChild(kwd_const);
            return term;
        }

        // array element | sunction call | method call
        if (auto var_name = Identifier())
        {
            auto term = new TreeElement("term");

            // variableName | functionName | className
            term->AddChild(var_name);

            // variableName[expression]
            if (auto square_brace_open = Symbol('['))
            {
                // push array
                codeGenerator.PushVar(static_cast<const IdentifierToken*>(var_name->getValue()));

                // [
                term->AddChild(square_brace_open);

                // expression
                term->AddChild(Expression());

                // ]
                term->AddChild(Symbol(']'));

                // add
                codeGenerator.Add();

                // pop pointer 1
                codeGenerator.PopPointer1();

                // push
                codeGenerator.PushThat0();
            }
            // method(expressionList)
            else if (auto open_brace = Symbol('('))
            {
                // push this as the first implicit argument to the method
                codeGenerator.PushThis();

                // (
                term->AddChild(open_brace);
                
                // expressionList
                auto param_list = term->AddChild(ExpressionList());

                // )
                term->AddChild(Symbol(')'));

                int n_explicit_args = std::count_if(param_list->getChildren().cbegin(), param_list->getChildren().cend(), [](TreeElement* x) { return x->getName() == "expression"; });
                // call method
                codeGenerator.Call(
                    /*name*/ codeGenerator.getClassName() + "." + token_value(*static_cast<const IdentifierToken*>(var_name->getValue())),
                    /*nArgs*/ 1 + n_explicit_args);
            }
            // (className | variableName).subroutineName(expressionList)
            else if (auto dot = Symbol('.'))
            {
                bool is_method = codeGenerator.IsVar(
                    /*name*/ static_cast<const IdentifierToken*>(var_name->getValue()));

                if (is_method)
                {
                    codeGenerator.PushVar(
                        static_cast<const IdentifierToken*>(var_name->getValue()));
                }

                // .
                term->AddChild(dot);

                // subroutine name
                auto subroutine_name = term->AddChild(Identifier());

                // (
                term->AddChild(Symbol('('));

                // expressionList
                auto param_list = term->AddChild(ExpressionList());

                // )
                term->AddChild(Symbol(')'));

                int n_explicit_args = std::count_if(param_list->getChildren().cbegin(), param_list->getChildren().cend(), [](TreeElement* x) { return x->getName() == "expression"; });
                // method
                if (is_method)
                {
                    auto var_type = codeGenerator.GetVarType(
                        static_cast<const IdentifierToken*>(var_name->getValue()));

                    codeGenerator.Call(
                        /*name*/ var_type + "." + token_value(*static_cast<const IdentifierToken*>(subroutine_name->getValue())),
                        /*nArgs*/ 1 + n_explicit_args);
                }
                // function
                else
                {
                    codeGenerator.Call(
                        /*name*/ token_value(*static_cast<const IdentifierToken*>(var_name->getValue())) + "." + token_value(*static_cast<const IdentifierToken*>(subroutine_name->getValue())),
                        /*nArgs*/ n_explicit_args);
                }
            }
            // variableName
            else
            {
                codeGenerator.PushVar(
                    static_cast<const IdentifierToken*>(var_name->getValue()));
            }

            return term;
        }

        // (expression)
        if (auto open_brace = Symbol('('))
        {
            auto term = new TreeElement("term");

            // (
            term->AddChild(open_brace);

            // expression
            term->AddChild(Expression());

            // )
            term->AddChild(Symbol(')'));

            return term;
        }

        // (- | ~) term
        if (auto unaryOp = UnaryOp())
        {
            auto term = new TreeElement("term");

            // (- | ~)
            term->AddChild(unaryOp);

            // term
            term->AddChild(Term());

            codeGenerator.UnaryOperator(
                static_cast<const SymbolToken*>(unaryOp->getValue()));

            return term;
        }

        return nullptr;
    }

    bool SubroutineCall(TreeElement* term)
    {
        if (auto name = Identifier())
        {
            // variableName | methodName | className
            term->AddChild(name);

            // method(expressionList)
            if (auto open_brace = Symbol('('))
            {
                // push this as the first implicit argument to the method
                codeGenerator.PushThis();

                // (
                term->AddChild(open_brace);

                // expressionList
                auto param_list = term->AddChild(ExpressionList());

                // )
                term->AddChild(Symbol(')'));

                int n_explicit_args = std::count_if(param_list->getChildren().cbegin(), param_list->getChildren().cend(), [](TreeElement* x) { return x->getName() == "expression"; });
                // call method
                codeGenerator.Call(
                    /*name*/ codeGenerator.getClassName() + "." + token_value(*static_cast<const IdentifierToken*>(name->getValue())),
                    /*nArgs*/ 1 + n_explicit_args);
            }
            // (className | variableName).subroutineName(expressionList)
            else if (auto dot = Symbol('.'))
            {
                bool is_method = codeGenerator.IsVar(
                    /*name*/ static_cast<const IdentifierToken*>(name->getValue()));

                if (is_method)
                {
                    codeGenerator.PushVar(
                        static_cast<const IdentifierToken*>(name->getValue()));
                }

                // .
                term->AddChild(dot);

                // subroutine name
                auto subroutine_name = term->AddChild(Identifier());

                // (
                term->AddChild(Symbol('('));

                // expressionList
                auto param_list = term->AddChild(ExpressionList());

                // )
                term->AddChild(Symbol(')'));

                int n_explicit_args = std::count_if(param_list->getChildren().cbegin(), param_list->getChildren().cend(), [](TreeElement* x) { return x->getName() == "expression"; });
                // method
                if (is_method)
                {
                    auto var_type = codeGenerator.GetVarType(
                        static_cast<const IdentifierToken*>(name->getValue()));

                    codeGenerator.Call(
                        /*name*/ var_type + "." + token_value(*static_cast<const IdentifierToken*>(subroutine_name->getValue())),
                        /*nArgs*/ 1 + n_explicit_args);
                }
                // function
                else
                {
                    codeGenerator.Call(
                        /*name*/ token_value(*static_cast<const IdentifierToken*>(name->getValue())) + "." + token_value(*static_cast<const IdentifierToken*>(subroutine_name->getValue())),
                        /*nArgs*/ n_explicit_args);
                }
            }

            return true;
        }

        return false;
    }

    TreeElement* ExpressionList()
    {
        auto expressionList = new TreeElement("expressionList");

        if (auto expression = Expression())
        {
            // expression
            expressionList->AddChild(expression);

            while (auto comma = Symbol(','))
            {
                // ,
                expressionList->AddChild(comma);

                // expression
                expressionList->AddChild(Expression());
            }
        }

        return expressionList;
    }
};