#pragma once

#include <string>
#include <ostream>
#include <sstream>
#include <map>
#include <stdexcept>

enum class TokenTypeEnum
{
    KEYWORD,
    SYMBOL,
    INTEGER_CONST,
    STRING_CONST,
    IDENTIFIER
};

enum class KeywordTypeEnum
{
    CLASS,
    METHOD,
    FUNCTION,
    CONSTRUCTOR,
    INT,
    BOOLEAN,
    CHAR,
    VOID,
    VAR,
    STATIC,
    FIELD,
    LET,
    DO,
    IF,
    ELSE,
    WHILE,
    RETURN,
    TRUE,
    FALSE,
    KWD_NULL,
    THIS
};

class Token
{
private:
    TokenTypeEnum tokenType;

public:
    Token(TokenTypeEnum tokenType)
        : tokenType(tokenType)
    { }

    virtual ~Token(){}

    const TokenTypeEnum getTokenType() const { return tokenType; }

    virtual std::ostream& write(std::ostream& os) const = 0;
};

// keyword
class KeywordToken : public Token
{
private:
    static std::map<const std::string, KeywordTypeEnum> KEYWORD_MAPPING;
    KeywordTypeEnum keywordType;

private:
    static void Init();

public:
    KeywordToken(KeywordTypeEnum keywordType)
        : Token(TokenTypeEnum::KEYWORD),
        keywordType(keywordType)
    { }

    const KeywordTypeEnum getKeywordType() const { return keywordType; }

    static bool is_keyword(const std::string& str)
    {
        Init();
        return KEYWORD_MAPPING.find(str) != KEYWORD_MAPPING.end();
    }

    static KeywordTypeEnum parse(const std::string& str)
    {
        Init();
        return KEYWORD_MAPPING.at(str);
    }

    std::ostream& write(std::ostream& os) const override;
};

// symbol
// { } ( ) [ ] . , ; + - * / & | < > = ~
class SymbolToken : public Token
{
private:
    static std::string SYMBOLS;
    char symbol;

public:
    SymbolToken(char symbol)
        : Token(TokenTypeEnum::SYMBOL),
        symbol(symbol)
    {
        if (!is_symbol(symbol))
            throw std::runtime_error("SymbolToken::SymbolToken: invalid symbol " + symbol);
    }

    const char getSymbol() const { return symbol; }

    static bool is_symbol(char symbol) { return SYMBOLS.find(symbol) != std::string::npos; }

    std::ostream& write(std::ostream& os) const override;
};

// identifier
// ( _ | [a-z] | [A-Z] ) ( _ | [0-9] | [a-z] | [A-Z] )*
class IdentifierToken : public Token
{
private:
    const std::string indentifier;

public:
    IdentifierToken(const std::string& indentifier)
        : Token(TokenTypeEnum::IDENTIFIER),
        indentifier(indentifier)
    { }

    const std::string& getIndentifier() const { return indentifier; }

    static bool is_identifier(const std::string& indentifier)
    {
        char c = indentifier[0];
        if (c != '_' && !std::isalpha(c))
            return false;

        for (size_t i = 1; i < indentifier.size(); i++)
        {
            c = indentifier[i];
            if (c != '_' && !std::isalpha(c) && !std::isdigit(c))
                return false;
        }
        
        return true;
    }

    std::ostream& write(std::ostream& os) const override;
};

// integer literal
// 0...32767
class IntegerConstToken : public Token
{
private:
    int intVal;

public:
    IntegerConstToken(int intVal)
        : Token(TokenTypeEnum::INTEGER_CONST),
        intVal(intVal)
    { }

    const int getIntVal() const { return intVal; }

    std::ostream& write(std::ostream& os) const override;
};

// string literal
// ".*"
class StringConstToken : public Token
{
private:
    const std::string stringVal;

public:
    // Creates a new string constant token.
    // Parameter [stringVal] is the string without surrounding quotes.
    StringConstToken(const std::string& stringVal)
        : Token(TokenTypeEnum::STRING_CONST),
        stringVal(stringVal)
    { }

    const std::string& getStringVal() const { return stringVal; }

    std::ostream& write(std::ostream& os) const override;
};

std::ostream& operator<<(std::ostream& os, TokenTypeEnum tokenType);

std::ostream& operator<<(std::ostream& os, KeywordTypeEnum keywordType);

inline std::string token_value(const Token& token)
{
    std::stringstream ss;
    token.write(ss);
    return ss.str();
}

inline std::string token_type(const Token& token)
{
    std::stringstream ss;
    ss << token.getTokenType();
    return ss.str();
}