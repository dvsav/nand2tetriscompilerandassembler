#include "Token.h"

std::map<const std::string, KeywordTypeEnum> KeywordToken::KEYWORD_MAPPING;

std::string SymbolToken::SYMBOLS("{}()[].,;+-*/&|<>=~");

void KeywordToken::Init()
{
    static bool initialized = false;
    if (initialized) return;
    initialized = true;

    KEYWORD_MAPPING["class"] = KeywordTypeEnum::CLASS;
    KEYWORD_MAPPING["method"] = KeywordTypeEnum::METHOD;
    KEYWORD_MAPPING["function"] = KeywordTypeEnum::FUNCTION;
    KEYWORD_MAPPING["constructor"] = KeywordTypeEnum::CONSTRUCTOR;
    KEYWORD_MAPPING["int"] = KeywordTypeEnum::INT;
    KEYWORD_MAPPING["boolean"] = KeywordTypeEnum::BOOLEAN;
    KEYWORD_MAPPING["char"] = KeywordTypeEnum::CHAR;
    KEYWORD_MAPPING["void"] = KeywordTypeEnum::VOID;
    KEYWORD_MAPPING["var"] = KeywordTypeEnum::VAR;
    KEYWORD_MAPPING["static"] = KeywordTypeEnum::STATIC;
    KEYWORD_MAPPING["field"] = KeywordTypeEnum::FIELD;
    KEYWORD_MAPPING["let"] = KeywordTypeEnum::LET;
    KEYWORD_MAPPING["do"] = KeywordTypeEnum::DO;
    KEYWORD_MAPPING["if"] = KeywordTypeEnum::IF;
    KEYWORD_MAPPING["else"] = KeywordTypeEnum::ELSE;
    KEYWORD_MAPPING["while"] = KeywordTypeEnum::WHILE;
    KEYWORD_MAPPING["return"] = KeywordTypeEnum::RETURN;
    KEYWORD_MAPPING["true"] = KeywordTypeEnum::TRUE;
    KEYWORD_MAPPING["false"] = KeywordTypeEnum::FALSE;
    KEYWORD_MAPPING["null"] = KeywordTypeEnum::KWD_NULL;
    KEYWORD_MAPPING["this"] = KeywordTypeEnum::THIS;
}

std::ostream& KeywordToken::write(std::ostream& os) const
{
    return os << keywordType;
}

std::ostream& SymbolToken::write(std::ostream& os) const
{
    switch (symbol)
    {
    case '<': return os << "&lt;";
    case '>': return os << "&gt;";
    case '"': return os << "&quot;";
    case '&': return os << "&amp;";
    }
    return os << symbol;
}

std::ostream& IdentifierToken::write(std::ostream& os) const
{
    return os << indentifier;
}

std::ostream& IntegerConstToken::write(std::ostream& os) const
{
    return os << intVal;
}

std::ostream& StringConstToken::write(std::ostream& os) const
{
    return os << stringVal;
}

std::ostream& operator<<(std::ostream& os, TokenTypeEnum tokenType)
{
    switch (tokenType)
    {
    case TokenTypeEnum::KEYWORD:
        os << "keyword";
        break;
    case TokenTypeEnum::SYMBOL:
        os << "symbol";
        break;
    case TokenTypeEnum::INTEGER_CONST:
        os << "integerConstant";
        break;
    case TokenTypeEnum::STRING_CONST:
        os << "stringConstant";
        break;
    case TokenTypeEnum::IDENTIFIER:
        os << "identifier";
        break;
    }

    return os;
}

std::ostream& operator<<(std::ostream& os, KeywordTypeEnum keywordType)
{
    switch (keywordType)
    {
    case KeywordTypeEnum::CLASS:
        os << "class";
        break;
    case KeywordTypeEnum::METHOD:
        os << "method";
        break;
    case KeywordTypeEnum::FUNCTION:
        os << "function";
        break;
    case KeywordTypeEnum::CONSTRUCTOR:
        os << "constructor";
        break;
    case KeywordTypeEnum::INT:
        os << "int";
        break;
    case KeywordTypeEnum::BOOLEAN:
        os << "boolean";
        break;
    case KeywordTypeEnum::CHAR:
        os << "char";
        break;
    case KeywordTypeEnum::VOID:
        os << "void";
        break;
    case KeywordTypeEnum::VAR:
        os << "var";
        break;
    case KeywordTypeEnum::STATIC:
        os << "static";
        break;
    case KeywordTypeEnum::FIELD:
        os << "field";
        break;
    case KeywordTypeEnum::LET:
        os << "let";
        break;
    case KeywordTypeEnum::DO:
        os << "do";
        break;
    case KeywordTypeEnum::IF:
        os << "if";
        break;
    case KeywordTypeEnum::ELSE:
        os << "else";
        break;
    case KeywordTypeEnum::WHILE:
        os << "while";
        break;
    case KeywordTypeEnum::RETURN:
        os << "return";
        break;
    case KeywordTypeEnum::TRUE:
        os << "true";
        break;
    case KeywordTypeEnum::FALSE:
        os << "false";
        break;
    case KeywordTypeEnum::KWD_NULL:
        os << "null";
        break;
    case KeywordTypeEnum::THIS:
        os << "this";
        break;
    }
    return os;
}

