#pragma once

#include <stdexcept>
#include <ostream>
#include <iostream>
#include <algorithm>

#include "Symbol.h"
#include "Token.h"

class CodeGenerator
{
private:
    std::ostream& os;
    std::string className;

    KeywordTypeEnum subroutineKind; // constructor | method | function
    std::string subroutineName;

    SymbolTable classSymbols;      // field | static
    SymbolTable subroutineSymbols; // argument | var

    int index_this;   // field
    int index_static; // static

    int index_argument; // argument
    int index_local;    // var

    int label_index;

public:
    CodeGenerator(std::ostream& os) :
        os(os),
        className(),
        subroutineKind(),
        subroutineName(),
        classSymbols(),
        subroutineSymbols(),
        index_this(),
        index_static(),
        index_argument(),
        index_local(),
        label_index()
    { }

private:
    const Symbol* FindSymbol(const std::string& name)
    {
        auto class_symbol = classSymbols.find(name);
        if (class_symbol != classSymbols.end())
            return &class_symbol->second;

        auto subroutine_symbol = subroutineSymbols.find(name);
        if (subroutine_symbol != subroutineSymbols.end())
            return &subroutine_symbol->second;

        return nullptr;
    }

public:
    void Class(const IdentifierToken* className)
    {
        this->className = className->getIndentifier();
    }

    void AddClassVariable(
        const KeywordToken* kind,    // FIELD | STATIC
        const Token* type,           // IdentifierToken | KeywordToken (CHAR | BOOLEAN | INT)
        const IdentifierToken* name) // name
    {
        int index{};

        SegmentEnum symbol_kind{};
        switch (kind->getKeywordType())
        {
        case KeywordTypeEnum::FIELD:
            symbol_kind = SegmentEnum::THIS;
            index = index_this++;
            break;

        case KeywordTypeEnum::STATIC:
            symbol_kind = SegmentEnum::STATIC;
            index = index_static++;
            break;

        default:
            throw std::runtime_error("AddClassVariable");
        }

        SymbolTypeEnum symbol_type{};
        switch (type->getTokenType())
        {
        case TokenTypeEnum::KEYWORD:
            switch (static_cast<const KeywordToken*>(type)->getKeywordType())
            {
            case KeywordTypeEnum::CHAR:
                symbol_type = SymbolTypeEnum::CHAR;
                break;

            case KeywordTypeEnum::BOOLEAN:
                symbol_type = SymbolTypeEnum::BOOLEAN;
                break;

            case KeywordTypeEnum::INT:
                symbol_type = SymbolTypeEnum::INT;
                break;

            default:
                throw std::runtime_error("AddClassVariable");
            }
            break;

        case TokenTypeEnum::IDENTIFIER:
            symbol_type = SymbolTypeEnum::CLASS;
            break;

        default:
            throw std::runtime_error("AddClassVariable");
        }

        classSymbols.emplace(
            name->getIndentifier(),
            Symbol(
              /*name*/ name->getIndentifier(),
              /*type*/ symbol_type,
              /*typeName*/ token_value(*type), 
              /*kind*/ symbol_kind,
              /*index*/ index));
    }

    void AddSubroutineLocalVariable(
        const Token* type,           // IdentifierToken | KeywordToken (CHAR | BOOLEAN | INT)
        const IdentifierToken* name) // name
    {
        SymbolTypeEnum symbol_type{};
        switch (type->getTokenType())
        {
        case TokenTypeEnum::KEYWORD:
            switch (static_cast<const KeywordToken*>(type)->getKeywordType())
            {
            case KeywordTypeEnum::CHAR:
                symbol_type = SymbolTypeEnum::CHAR;
                break;

            case KeywordTypeEnum::BOOLEAN:
                symbol_type = SymbolTypeEnum::BOOLEAN;
                break;

            case KeywordTypeEnum::INT:
                symbol_type = SymbolTypeEnum::INT;
                break;

            default:
                throw std::runtime_error("AddClassVariable");
            }
            break;

        case TokenTypeEnum::IDENTIFIER:
            symbol_type = SymbolTypeEnum::CLASS;
            break;

        default:
            throw std::runtime_error("AddClassVariable");
        }

        subroutineSymbols.emplace(
            name->getIndentifier(),
            Symbol(
              /*name*/ name->getIndentifier(),
              /*type*/ symbol_type,
              /*typeName*/ token_value(*type),
              /*segment*/ SegmentEnum::LOCAL,
              /*index*/ index_local++));
    }

    void AddSubroutineParameter(
        const Token* type,           // IdentifierToken | KeywordToken (CHAR | BOOLEAN | INT)
        const IdentifierToken* name) // name
    {
        SymbolTypeEnum symbol_type{};
        switch (type->getTokenType())
        {
        case TokenTypeEnum::KEYWORD:
            switch (static_cast<const KeywordToken*>(type)->getKeywordType())
            {
            case KeywordTypeEnum::CHAR:
                symbol_type = SymbolTypeEnum::CHAR;
                break;

            case KeywordTypeEnum::BOOLEAN:
                symbol_type = SymbolTypeEnum::BOOLEAN;
                break;

            case KeywordTypeEnum::INT:
                symbol_type = SymbolTypeEnum::INT;
                break;

            default:
                throw std::runtime_error("AddClassVariable");
            }
            break;

        case TokenTypeEnum::IDENTIFIER:
            symbol_type = SymbolTypeEnum::CLASS;
            break;

        default:
            throw std::runtime_error("AddClassVariable");
        }

        subroutineSymbols.emplace(
            name->getIndentifier(),
            Symbol(
              /*name*/ name->getIndentifier(),
              /*type*/ symbol_type,
              /*typeName*/ token_value(*type),
              /*segment*/ SegmentEnum::ARGUMENT,
              /*index*/ index_argument++));
    }

    void BeginSubroutineDeclaration(
        const KeywordToken* kind,    // constructor | function | method
        const IdentifierToken* name) // name
    {
        subroutineSymbols.clear();
        index_argument = 0;
        index_local = 0;

        subroutineKind = kind->getKeywordType();

        switch (subroutineKind)
        {
        case KeywordTypeEnum::METHOD:
        {
            subroutineSymbols.emplace(
                "this",
                Symbol(
                    /*name*/ "this",
                    /*type*/ SymbolTypeEnum::CLASS,
                    /*typeName*/ className,
                    /*segment*/ SegmentEnum::ARGUMENT,
                    /*index*/ index_argument++));

            break;
        }

        case KeywordTypeEnum::CONSTRUCTOR:
        case KeywordTypeEnum::FUNCTION:
            break;

        default:
            throw std::runtime_error("Subroutine");
        }

        subroutineName = name->getIndentifier();
    }

    void EndSubroutineDeclaration()
    {
        os << "function " << className << '.' << subroutineName << ' '
            << std::count_if(subroutineSymbols.begin(), subroutineSymbols.end(), [](SymbolTable::value_type& pair) { return pair.second.Segment() == SegmentEnum::LOCAL; })
            << std::endl;

        switch (subroutineKind)
        {
        case KeywordTypeEnum::CONSTRUCTOR:
            PushConst(std::count_if(classSymbols.begin(), classSymbols.end(), [](SymbolTable::value_type& pair) { return pair.second.Segment() == SegmentEnum::THIS; }));
            os << "call Memory.alloc 1" << std::endl;
            os << "pop pointer 0" << std::endl;
            break;

        case KeywordTypeEnum::METHOD:
            os << "push argument 0" << std::endl;
            os << "pop pointer 0" << std::endl;
            break;
        }
    }

    void PushVar(const IdentifierToken* name)
    {
        auto symbol = FindSymbol(name->getIndentifier());
        if(symbol)
            os << "push " << symbol->Segment() << ' ' << symbol->Index() << std::endl;
        else
            throw std::runtime_error("PushVar");
    }

    void PopVar(const IdentifierToken* name)
    {
        auto symbol = FindSymbol(name->getIndentifier());
        if (symbol)
            os << "pop " << symbol->Segment() << ' ' << symbol->Index() << std::endl;
        else
            throw std::runtime_error("PopVar");
    }

    void PushConst(int value)
    {
        os << "push constant " << value << std::endl;
    }

    void PushKeywordConst(const KeywordToken* value)
    {
        switch (value->getKeywordType())
        {
        case KeywordTypeEnum::TRUE:
            PushConst(0);
            Not();
            break;

        case KeywordTypeEnum::FALSE:
        case KeywordTypeEnum::KWD_NULL:
            PushConst(0);
            break;

        case KeywordTypeEnum::THIS:
            PushThis();
            break;

        default:
            throw std::runtime_error("PushKeywordConst");
        }
    }

    void Add()
    {
        os << "add" << std::endl;
    }

    bool IsVar(const IdentifierToken* name)
    {
        return FindSymbol(name->getIndentifier());
    }

    const std::string GetVarType(const IdentifierToken* name)
    {
        auto symbol = FindSymbol(name->getIndentifier());
        if (symbol)
            return symbol->TypeName();
        else
            throw std::runtime_error("GetVarType");
    }

    void PushThis()
    {
        os << "push pointer 0" << std::endl;
    }

    void PushTemp0()
    {
        os << "push temp 0" << std::endl;
    }

    void PushThat0()
    {
        os << "push that 0" << std::endl;
    }

    void Return()
    {
        os << "return" << std::endl;
    }

    void PopTemp0()
    {
        os << "pop temp 0" << std::endl;
    }

    void PopPointer1()
    {
        os << "pop pointer 1" << std::endl;
    }

    void PopThat0()
    {
        os << "pop that 0" << std::endl;
    }

    void PushStringConst(const std::string& value)
    {
        PushConst(value.length());
        Call("String.new", 1);

        for (auto c : value)
        {
            PushConst(c);
            Call("String.appendChar", 2);
        }
    }

    void BinaryOperator(
        const SymbolToken* op)
    {
        // + - * / & | < > =
        switch (op->getSymbol())
        {
        case '+':
            os << "add" << std::endl;
            break;

        case '-':
            os << "sub" << std::endl;
            break;

        case '*':
            os << "call Math.multiply 2" << std::endl;
            break;

        case '/':
            os << "call Math.divide 2" << std::endl;
            break;

        case '&':
            os << "and" << std::endl;
            break;

        case '|':
            os << "or" << std::endl;
            break;

        case '<':
            os << "lt" << std::endl;
            break;

        case '>':
            os << "gt" << std::endl;
            break;

        case '=':
            os << "eq" << std::endl;
            break;

        default:
            throw std::runtime_error("BinaryOperator");
        }
    }

    void UnaryOperator(
        const SymbolToken* op)
    {
        // - ~
        switch (op->getSymbol())
        {
        case '-':
            os << "neg" << std::endl;
            break;

        case '~':
            os << "not" << std::endl;
            break;

        default:
            throw std::runtime_error("UnaryOperator");
        }
    }

    void Call(
        const std::string& name,
        int nArgs)
    {
        os << "call " << name << ' ' << nArgs << std::endl;
    }

    const std::string& getClassName() const
    {
        return className;
    }

    const int EmitLabelIndex()
    {
        return label_index++;
    }

    void Not()
    {
        os << "not" << std::endl;
    }

    void IfGoTo(const std::string& label)
    {
        os << "if-goto " << label << std::endl;
    }

    void GoTo(const std::string& label)
    {
        os << "goto " << label << std::endl;
    }

    void Label(const std::string& label)
    {
        os << "label " << label << std::endl;
    }
};