#pragma once

#include <vector>
#include <stdexcept>
#include <ostream>
#include <iostream>

#include "Token.h"

class TreeElement
{
private:
    const std::string name;
    const Token* value;
    std::vector<TreeElement*> children;

public:
    explicit TreeElement(const std::string& name) noexcept :
        name(name),
        value(nullptr),
        children()
    {
#ifdef DEBUG
        std::cout << name << std::endl;
#endif // DEBUG
    }

    explicit TreeElement(const Token* token) noexcept :
        name(token_type(*token)),
        value(token)
    {
#ifdef DEBUG
        std::cout << name << " = " << token_value(*value) << std::endl;
#endif // DEBUG
    }

    TreeElement(TreeElement&& elem) noexcept :
        name(elem.name),
        value(elem.value),
        children(std::forward< std::vector<TreeElement*> >(elem.children))
    { }

public:
    const std::string& getName() const noexcept
    {
        return name;
    }

    const Token* getValue() const noexcept
    {
        return value;
    }

    TreeElement* AddChild(TreeElement* child)
    {
        if (!child)
            throw new std::runtime_error(name);

        children.push_back(child);
        return child;
    }

    const std::vector<TreeElement*>& getChildren() const noexcept
    {
        return children;
    }
};

void TreeElementWriteXml(
    const TreeElement& root,
    std::ostream& os,
    int indent)
{
    if (root.getValue())
    {
        os << std::string(indent, ' ');
        os << '<' << root.getName() << "> ";
        root.getValue()->write(os);
        os << " </" << root.getName() << '>';
        os << std::endl;
    }
    else
    {
        os << std::string(indent, ' ') << '<' << root.getName() << '>' << std::endl;

        for (auto child : root.getChildren())
            TreeElementWriteXml(*child, os, indent + 2);

        os << std::string(indent, ' ') << "</" << root.getName() << '>' << std::endl;
    }
}

void TreeElementFree(
    TreeElement* root)
{
    for (auto child : root->getChildren())
        TreeElementFree(child);

    delete root;
}