#pragma once

#include <vector>
#include <istream>

#include "Token.h"

class Tokenizer
{
public:
    static std::vector<Token*> Tokenize(
        std::istream& is)
    {
        std::vector<Token*> tokens;

        while (true)
        {
            // End Of File
            if (!is) return tokens;

            // Probe single character in the stream 
            char c = is.get();

            // End of line or other whitespace symbol
            if (std::isspace(c)) continue;

            // Comment
            if (c == '/')
            {
                char c2 = is.get();

                if (c2 == '/')      // "//" single line comment
                {
                    while (is.get() != '\n')
                    {
                        if (!is) return tokens;
                    };
                    continue;
                }
                else if (c2 == '*') // "/* ... */" multiline comment
                {
                    while (true)
                    {
                        while (is.get() != '*')
                        {
                            if (!is)
                                throw std::runtime_error("Tokenizer::Tokenize(): comment is not closed");
                        };

                        if (!is)
                            throw std::runtime_error("Tokenizer::Tokenize(): comment is not closed");

                        if (is.get() == '/')
                            break;
                    }
                    continue;
                }
                else
                    is.unget();
            }

            // Symbol
            if (SymbolToken::is_symbol(c))
            {
                tokens.push_back(new SymbolToken(c));
                continue;
            }

            // Integer constant
            if (std::isdigit(c))
            {
                is.unget();
                int int_val = 0;
                is >> int_val;
                tokens.push_back(new IntegerConstToken(int_val));
                continue;
            }

            // String constant
            if (c == '"')
            {
                std::string string_val;
                while ((c = is.get()) != '"')
                {
                    if (!is)
                        throw std::runtime_error("Tokenizer::Tokenize(): missing quote");

                    string_val.push_back(c);
                }

                tokens.push_back(new StringConstToken(string_val));
                continue;
            }

            // Identifier or Keyword
            std::string identifier_or_keyword;
            identifier_or_keyword.push_back(c);
            while (true)
            {
                if (!is) return tokens;

                c = is.get();
                if (std::isspace(c))
                {
                    break;
                }
                else if (SymbolToken::is_symbol(c))
                {
                    is.unget();
                    break;
                }
                identifier_or_keyword.push_back(c);
            }

            // Keyword
            if (KeywordToken::is_keyword(identifier_or_keyword))
            {
                tokens.push_back(new KeywordToken(KeywordToken::parse(identifier_or_keyword)));
                continue;
            }
            
            // Identifier
            if (IdentifierToken::is_identifier(identifier_or_keyword))
            {
                tokens.push_back(new IdentifierToken(identifier_or_keyword));
                continue;
            }

            throw std::runtime_error("Tokenizer::Tokenize(): unexpected symbol");
        }

        return tokens;
    }

    static void DisposeTokens(
        std::vector<Token*> tokens)
    {
        for (auto token : tokens)
        {
            delete token;
        }

        tokens.clear();
    }

    static void WriteTokenToXml(
        const Token& token,
        std::ostream& os)
    {
        os << "<" << token.getTokenType() << "> ";
        token.write(os);
        os << " </" << token.getTokenType() << ">";
    }

    static void WriteTokensToXml(
        std::vector<Token*> tokens,
        std::ostream& os)
    {
        os << "<tokens>" << std::endl;

        for (auto token : tokens)
        {
            WriteTokenToXml(*token, os);
            os << std::endl;
        }

        os << "</tokens>" << std::endl;
    }
};