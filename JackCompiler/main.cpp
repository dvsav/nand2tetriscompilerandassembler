#include <iostream>
#include <fstream>
#include <sstream>

#include "Tokenizer.h"
#include "Parser.h"
#include "CodeGenerator.h"

#if !_HAS_CXX17
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #include <filesystem>
    namespace fs = std::filesystem;
#endif

int main(int argc, char** argv)
{
    std::string path;

    if (argc == 2)
        path = argv[1];
    else
        path = fs::current_path().string();

    if (fs::is_directory(path))
    {
        for (auto& file : fs::directory_iterator(path))
        {
            if (file.path().extension() == ".jack")
            {
                std::ifstream ifs(file.path());
                auto tokens = Tokenizer::Tokenize(ifs);

                auto vm_file = file.path();
                vm_file.replace_extension(".vm");
                std::ofstream ofs(vm_file);

                CodeGenerator cg(ofs);
                Parser parser(cg, tokens.begin(), tokens.end());
                auto syntax_tree = parser.Parse();
                TreeElementFree(syntax_tree);

                Tokenizer::DisposeTokens(tokens);
            }
        }
    }
    else
    {
        std::ifstream ifs(path);
        auto tokens = Tokenizer::Tokenize(ifs);

        auto vm_file = fs::path(path);
        vm_file.replace_extension(".vm");
        std::ofstream ofs(vm_file);

        CodeGenerator cg(std::cout);
        Parser parser(cg, tokens.begin(), tokens.end());
        auto syntax_tree = parser.Parse();
        TreeElementFree(syntax_tree);

        Tokenizer::DisposeTokens(tokens);
    }

    return 0;
}
