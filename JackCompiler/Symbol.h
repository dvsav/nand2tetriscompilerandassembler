#pragma once

#include <string>
#include <map>
#include <ostream>
#include <iomanip>

#include "Token.h"

enum class SymbolTypeEnum
{
    CHAR,
    BOOLEAN,
    INT,
    CLASS
};

enum class SegmentEnum
{
    ARGUMENT,
    THIS,
    STATIC,
    LOCAL
};

class Symbol
{
private:
    std::string name;     // variable name
    SymbolTypeEnum type;  // CLASS | CHAR | BOOLEAN | INT
    std::string typeName; // ClassName | char | boolean | int
    SegmentEnum segment;  // ARGUMENT | FIELD | STATIC | LOCAL
    int index;            // index inside segment (argument | this | static | local)

public:
    Symbol(
        const std::string& name,
        SymbolTypeEnum type,
        const std::string& typeName,
        SegmentEnum segment,
        int index) :

        name(name),
        type(type),
        typeName(typeName),
        segment(segment),
        index(index)
    { }

public:
    const std::string& Name() const { return name; }
    const SymbolTypeEnum Type() const { return type; }
    const std::string& TypeName() const { return typeName; }
    const SegmentEnum Segment() const { return segment; }
    const int Index() const { return index; }
};

using SymbolTable = std::map<const std::string, Symbol>;

inline std::ostream& operator<<(std::ostream& os, SegmentEnum segment)
{
    switch (segment)
    {
    case SegmentEnum::ARGUMENT:
        os << "argument";
        break;

    case SegmentEnum::LOCAL:
        os << "local";
        break;

    case SegmentEnum::THIS:
        os << "this";
        break;

    case SegmentEnum::STATIC:
        os << "static";
        break;
    }

    return os;
}

inline std::ostream& operator<<(std::ostream& os, const Symbol& symbol)
{
    os << std::setw(20) << symbol.Name();
    os << std::setw(10) << symbol.TypeName();
    os << std::setw(10) << symbol.Segment();
    os << std::setw(10) << symbol.Index();
    return os;
}