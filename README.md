# Nand2TetrisCompilerAndAssembler

Nand2TetrisCompilerAndAssembler repository has two projects inside: assembler for Hack assembly language and compiler for Jack high-level object oriented Java-like language.

For more information about Hack computer and Jack language see [From Nand to Tetris](https://www.nand2tetris.org).
For brief information about this project in Russian language see [the project author's blog](https://dvsav.ru/nand2tetris/).  

For downloading source code that can be compiled by HackAssembler and JackCompiler refer to [Build a Modern Computer from First Principles: Nand to Tetris Part II (project-centered course)](https://coursera.org/learn/nand2tetris2).

There's also a project inside the repository that is called "Windows" written in Jack, that you can compile with [BuildWindows.bat](BuildWindows.bat) batch script. This script first runs the compiler that translates [*.jack] files from [Windows](Windows) folder into [*.vm] assembly language files. Next it runs the assembler that translates [*.vm] files into single [.asm] file which can further be executed by the Virtual Machine Emulator which can be downloaded from [From Nand to Tetris](https://www.nand2tetris.org).

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
