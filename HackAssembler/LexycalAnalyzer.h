#pragma once

#include <vector>
#include <istream>
#include <stdexcept>

#include "Token.h"

class LexycalAnalyzer
{
public:
    static std::vector<Token> Analyze(
        std::istream& is)
    {
        std::vector<Token> tokens;
        int line_number = 0;

        while (true)
        {
            line_number++;

            char c = 0;
            int number = 0;

            // ** Probe single character in the stream **
            c = is.get();
            if (is.eof()) // End Of File
                return tokens;
            else if(is.bad())
                throw std::runtime_error("LexycalAnalyzer::Analyze(): stream ran into bad state");

            // ** End of line or other whitespace symbol **
            if (std::isspace(c))
            {
                if (c == '\n')
                    tokens.push_back(Token(TokenType::ENDL, "\n"));
                continue;
            }
            is.unget();

            // ** Digit **
            if (std::isdigit(c)) // integer number
            {
                is >> number;
                tokens.push_back(Token(TokenType::NUMBER, std::to_string(number)));
            }
            else if (c == '/') // comment
            {
                is.get();
                if (!(is >> c) || c != '/')
                    throw std::runtime_error("LexycalAnalyzer::Analyze(): Syntax error. Line " + std::to_string(line_number));
                
                while (is.get() != '\n') {};
                tokens.push_back(Token(TokenType::ENDL, "\n"));
            }
            else
            {
                std::string keyword;
                is >> keyword;

                if (keyword == "push" ||
                    keyword == "pop")
                {
                    tokens.push_back(Token(TokenType::MEMORY_ACCESS, keyword));
                }
                else if (
                    keyword == "local" ||
                    keyword == "argument" ||
                    keyword == "this" ||
                    keyword == "that" ||
                    keyword == "constant" ||
                    keyword == "static" ||
                    keyword == "pointer" ||
                    keyword == "temp")
                {
                    tokens.push_back(Token(TokenType::SEGMENT, keyword));
                }
                else if (
                    keyword == "add" ||
                    keyword == "sub" ||
                    keyword == "neg" ||
                    keyword == "eq" ||
                    keyword == "gt" ||
                    keyword == "lt" ||
                    keyword == "and" ||
                    keyword == "or" ||
                    keyword == "not")
                {
                    tokens.push_back(Token(TokenType::OPERATION, keyword));
                }
                else if (keyword == "goto")
                {
                    tokens.push_back(Token(TokenType::GOTO, keyword));
                }
                else if (keyword == "if-goto")
                {
                    tokens.push_back(Token(TokenType::IFGOTO, keyword));
                }
                else if (keyword == "label")
                {
                    tokens.push_back(Token(TokenType::LABEL, keyword));
                }
                else if (keyword == "call")
                {
                    tokens.push_back(Token(TokenType::CALL, keyword));
                }
                else if (keyword == "function")
                {
                    tokens.push_back(Token(TokenType::FUNCTION, keyword));
                }
                else if (keyword == "return")
                {
                    tokens.push_back(Token(TokenType::RETURN, keyword));
                }
                else
                {
                    tokens.push_back(Token(TokenType::SYMBOL, keyword));
                }
            }
        }
    }
};