#pragma once

#include <vector>
#include <stdexcept>
#include <ostream>

#include "Token.h"

class CommandTranslator
{
private:
    const std::string& m_ClassName;
    const std::vector<Token>& m_Tokens;
    std::ostream& m_Output;
    size_t m_CurrentTokenIndex = 0;
    int m_JumpIndex = 0;
    int m_InstructionIndex = 0;

public:
    CommandTranslator(
        const std::string& className,
        const std::vector<Token>& tokens,
        std::ostream& os) :
        m_ClassName(className),
        m_Tokens(tokens),
        m_Output(os)
    { }

private:
    const Token& CurrentToken() { return m_Tokens[m_CurrentTokenIndex]; }

    void MoveToNextToken()
    {
        if (m_CurrentTokenIndex < m_Tokens.size() - 1)
            m_CurrentTokenIndex++;
        else
        {
            throw std::runtime_error("Token series is incomplete. Last found token is "
                + std::to_string(m_CurrentTokenIndex) + " " + CurrentToken().getValue());
        }
    }

    void TranslationLoop()
    {
        while (true)
        {
            bool ok = false;

            if (Push())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size()-1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Pop())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size()-1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Arithmetic())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size()-1)
                    MoveToNextToken();
                else
                    break;
            }

            if (EndLine())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size()-1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Goto())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (IfGoto())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Label())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Call())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Function())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (Return())
            {
                ok = true;
                if (m_CurrentTokenIndex < m_Tokens.size() - 1)
                    MoveToNextToken();
                else
                    break;
            }

            if (!ok)
            {
                throw std::runtime_error("Unrecognized sequence of tokens. Last found token is "
                    + std::to_string(m_CurrentTokenIndex) + " " + CurrentToken().getValue());
            }
        }
    }

    bool Push()
    {
        if (CurrentToken().getTokenType() == TokenType::MEMORY_ACCESS)
        {
            if (CurrentToken().getValue() == "push")
            {
                MoveToNextToken();
                const std::string segment = Segment();

                MoveToNextToken();
                int number = Number();

                MoveToNextToken();
                if (!EndLine())
                    ThrowSyntaxError();

                // ** translate "push segment number" **
                m_Output << "// " << m_InstructionIndex << std::endl;
                m_Output << "// push " << segment << " " << number << std::endl;
                TranslatePush(segment, number);
                m_Output << std::endl;

                return true;
            }
        }

        return false;
    }

    bool Pop()
    {
        if (CurrentToken().getTokenType() == TokenType::MEMORY_ACCESS)
        {
            if (CurrentToken().getValue() == "pop")
            {
                MoveToNextToken();
                const std::string segment = Segment();

                MoveToNextToken();
                int number = Number();

                MoveToNextToken();
                if (!EndLine())
                    ThrowSyntaxError();

                // ** translate "pop segment number" **
                m_Output << "// " << m_InstructionIndex << std::endl;
                m_Output << "// pop " << segment << " " << number << std::endl;
                TranslatePop(segment, number);
                m_Output << std::endl;

                return true;
            }
        }

        return false;
    }

    bool Arithmetic()
    {
        if (CurrentToken().getTokenType() == TokenType::OPERATION)
        {
            // ** translate "add | sub | neg | eq | gt | lt | and | or | not" **
            const std::string operation = CurrentToken().getValue();

            MoveToNextToken();
            if(!EndLine())
                ThrowSyntaxError();

            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// " << operation << std::endl;
            TranslateArithmetic(operation);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool IfGoto()
    {
        if (CurrentToken().getTokenType() == TokenType::IFGOTO)
        {
            MoveToNextToken();
            std::string label = Symbol();

            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            // ** translate "if-goto label" **
            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// if-goto " << label << std::endl;
            TranslateIfGoto(label);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool Goto()
    {
        if (CurrentToken().getTokenType() == TokenType::GOTO)
        {
            MoveToNextToken();
            std::string label = Symbol();

            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            // ** translate "goto label" **
            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// goto " << label << std::endl;
            TranslateGoto(label);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool Label()
    {
        if (CurrentToken().getTokenType() == TokenType::LABEL)
        {
            MoveToNextToken();
            std::string label = Symbol();

            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            m_Output << "// label " << label << std::endl;
            TranslateLabel(label);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool Call()
    {
        if (CurrentToken().getTokenType() == TokenType::CALL)
        {
            MoveToNextToken();
            std::string functionName = Symbol();

            MoveToNextToken();
            int nArgs = Number();

            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            // ** translate "call functionName nArgs" **
            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// call " << functionName << " " << nArgs << std::endl;
            TranslateCall(functionName, nArgs);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool Function()
    {
        if (CurrentToken().getTokenType() == TokenType::FUNCTION)
        {
            MoveToNextToken();
            std::string functionName = Symbol();

            MoveToNextToken();
            int nVars = Number();

            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            // ** translate "function functionName nVars" **
            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// function " << functionName << " " << nVars << std::endl;
            TranslateFunction(functionName, nVars);
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    bool Return()
    {
        if (CurrentToken().getTokenType() == TokenType::RETURN)
        {
            MoveToNextToken();
            if (!EndLine())
                ThrowSyntaxError();

            // ** translate "return" **
            m_Output << "// " << m_InstructionIndex << std::endl;
            m_Output << "// return" << std::endl;
            TranslateReturn();
            m_Output << std::endl;

            return true;
        }
        return false;
    }

    const std::string Segment()
    {
        if (CurrentToken().getTokenType() != TokenType::SEGMENT)
            ThrowSyntaxError();

        return CurrentToken().getValue();
    }

    int Number()
    {
        if (CurrentToken().getTokenType() != TokenType::NUMBER)
            ThrowSyntaxError();

        return std::stoi(CurrentToken().getValue());
    }

    bool EndLine()
    {
        return (CurrentToken().getTokenType() == TokenType::ENDL);
    }

    const std::string& Symbol()
    {
        if (CurrentToken().getTokenType() != TokenType::SYMBOL)
            ThrowSyntaxError();

        return CurrentToken().getValue();
    }

private:
    void ThrowSyntaxError()
    {
        throw std::runtime_error("Syntax Error. Last found token is "
            + std::to_string(m_CurrentTokenIndex) + " " + CurrentToken().getValue());
    }

    static constexpr int temp(int i) { return 5 + i; }

    static constexpr int reg(int i) { return 13 + i; }

    void TranslatePush(
        const std::string& segment,
        int address)
    {
        if (segment == "constant")
        {
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "local")
        {
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;

            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "A=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "argument")
        {
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;

            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "A=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "this")
        {
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;

            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "A=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "that")
        {
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;

            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "A=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "static")
        {
            m_Output << "@" << m_ClassName << "." << address << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "temp")
        {
            m_Output << "@" << temp(address) << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "pointer")
        {
            if (address == 0)
            {
                m_Output << "@THIS" << std::endl; m_InstructionIndex++;
                m_Output << "D=M" << std::endl; m_InstructionIndex++;
            }
            else if (address == 1)
            {
                m_Output << "@THAT" << std::endl; m_InstructionIndex++;
                m_Output << "D=M" << std::endl; m_InstructionIndex++;
            }
            else
                ThrowSyntaxError();
        }

        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;
    }

    void TranslatePop(
        const std::string& segment,
        int address)
    {
        if (segment == "local")
        {
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=A-D" << std::endl; m_InstructionIndex++;
            m_Output << "@LCL" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "argument")
        {
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=A-D" << std::endl; m_InstructionIndex++;
            m_Output << "@ARG" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "this")
        {
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=A-D" << std::endl; m_InstructionIndex++;
            m_Output << "@THIS" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "that")
        {
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=D+A" << std::endl; m_InstructionIndex++;
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@" << address << std::endl; m_InstructionIndex++;
            m_Output << "D=A" << std::endl; m_InstructionIndex++;
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "D=A-D" << std::endl; m_InstructionIndex++;
            m_Output << "@THAT" << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "static")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;

            m_Output << "@" << m_ClassName << "." << address << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "temp")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;

            m_Output << "@" << temp(address) << std::endl; m_InstructionIndex++;
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
        }
        else if (segment == "pointer")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
        
            if (address == 0)
            {
                m_Output << "@THIS" << std::endl; m_InstructionIndex++;
                m_Output << "M=D" << std::endl; m_InstructionIndex++;
            }
            else if (address == 1)
            {
                m_Output << "@THAT" << std::endl; m_InstructionIndex++;
                m_Output << "M=D" << std::endl; m_InstructionIndex++;
            }
            else
                ThrowSyntaxError();
        }
        else if (segment == "constant")
            ThrowSyntaxError();
    }

    void TranslateArithmetic(
        const std::string& operation)
    {
        if (operation == "add")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=D+M" << std::endl; m_InstructionIndex++;
        }
        else if(operation == "sub")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=M-D" << std::endl; m_InstructionIndex++;
        }
        else if (operation == "eq")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "MD=M-D" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".True" << std::endl; m_InstructionIndex++;
            m_Output << "D;JEQ" << std::endl; m_InstructionIndex++;

            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=0" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".False" << std::endl; m_InstructionIndex++;
            m_Output << "0;JMP" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".True)" << std::endl;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=-1" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".False)" << std::endl;

            m_JumpIndex++;
        }
        else if (operation == "gt")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "MD=M-D" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".True" << std::endl; m_InstructionIndex++;
            m_Output << "D;JGT" << std::endl; m_InstructionIndex++;

            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=0" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".False" << std::endl; m_InstructionIndex++;
            m_Output << "0;JMP" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".True)" << std::endl;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=-1" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".False)" << std::endl;

            m_JumpIndex++;
        }
        else if (operation == "lt")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "MD=M-D" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".True" << std::endl; m_InstructionIndex++;
            m_Output << "D;JLT" << std::endl; m_InstructionIndex++;

            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=0" << std::endl; m_InstructionIndex++;
            m_Output << "@JUMP." << m_ClassName << "." << m_JumpIndex << ".False" << std::endl; m_InstructionIndex++;
            m_Output << "0;JMP" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".True)" << std::endl;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;
            m_Output << "M=-1" << std::endl; m_InstructionIndex++;

            m_Output << "(JUMP." << m_ClassName << "." << m_JumpIndex << ".False)" << std::endl;

            m_JumpIndex++;
        }
        else if (operation == "and")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=M&D" << std::endl; m_InstructionIndex++;
        }
        else if (operation == "or")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
            m_Output << "D=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=M|D" << std::endl; m_InstructionIndex++;
        }
        else if (operation == "neg")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=-M" << std::endl; m_InstructionIndex++;
        }
        else if (operation == "not")
        {
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "A=M" << std::endl; m_InstructionIndex++;
            m_Output << "A=A-1" << std::endl; m_InstructionIndex++;

            m_Output << "M=!M" << std::endl; m_InstructionIndex++;
        }
        else
            ThrowSyntaxError();
    }

    void TranslateGoto(
        const std::string& label)
    {
        m_Output << "@" << label << std::endl; m_InstructionIndex++;
        m_Output << "0;JMP" << std::endl; m_InstructionIndex++;
    }

    void TranslateIfGoto(
        const std::string& label)
    {
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@" << label << std::endl; m_InstructionIndex++;
        m_Output << "D;JNE" << std::endl; m_InstructionIndex++;
    }

    void TranslateLabel(
        const std::string& label)
    {
        m_Output << "(" << label << ")" << std::endl;
    }

    void TranslateCall(
        const std::string& functionName,
        int nArgs)
    {
        // 1.1 push return address
        std::string return_label = "RET." + m_ClassName + "." + std::to_string(m_JumpIndex++);
        m_Output << "@" << return_label << std::endl; m_InstructionIndex++;
        m_Output << "D=A" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;

        // 1.2 push LCL
        m_Output << "@LCL" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;

        // 1.3 push ARG
        m_Output << "@ARG" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;

        // 1.4 push THIS
        m_Output << "@THIS" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;

        // 1.5 push THAT
        m_Output << "@THAT" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=M+1" << std::endl; m_InstructionIndex++;

        // 2. ARG = SP - nArgs
        m_Output << "@" << (nArgs+5) << std::endl; m_InstructionIndex++;
        m_Output << "D=A" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "D=M-D" << std::endl; m_InstructionIndex++;
        m_Output << "@ARG" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 3. LCL = SP
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@LCL" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 4. goto functionName
        m_Output << "@" << functionName << std::endl; m_InstructionIndex++;
        m_Output << "0;JMP" << std::endl; m_InstructionIndex++;

        // 5. return label
        m_Output << "(" << return_label << ")" << std::endl;
    }

    void TranslateFunction(
        const std::string& functionName,
        int nVars)
    {
        // 1. function label
        m_Output << "(" << functionName << ")" << std::endl;

        // 2. push 0 nVars times
        m_Output << "@0" << std::endl; m_InstructionIndex++;
        m_Output << "D=A" << std::endl; m_InstructionIndex++;

        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;

        for (int i = 0; i < nVars; i++)
        {
            m_Output << "M=D" << std::endl; m_InstructionIndex++;
            m_Output << "@SP" << std::endl; m_InstructionIndex++;
            m_Output << "AM=M+1" << std::endl; m_InstructionIndex++;
        }
    }

    void TranslateReturn()
    {
#define endFrame 0
#define retAddr 1

        // 1. reg(endFrame) = LCL
        m_Output << "@LCL" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 2. reg(retAddr) = *(reg(endFrame) - 5)
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@5" << std::endl; m_InstructionIndex++;
        m_Output << "A=D-A" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@" << reg(retAddr) << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        //3. *ARG = pop()
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "A=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@ARG" << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 4. SP = ARG + 1
        m_Output << "@ARG" << std::endl; m_InstructionIndex++;
        m_Output << "D=M+1" << std::endl; m_InstructionIndex++;
        m_Output << "@SP" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 5. THAT = *(reg(endFrame) - 1)
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@THAT" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 6. THIS = *(reg(endFrame) - 2)
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@THIS" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 7. ARG = *(reg(endFrame) - 3)
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@ARG" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 8. LCL = *(reg(endFrame) - 4)
        m_Output << "@" << reg(endFrame) << std::endl; m_InstructionIndex++;
        m_Output << "AM=M-1" << std::endl; m_InstructionIndex++;
        m_Output << "D=M" << std::endl; m_InstructionIndex++;
        m_Output << "@LCL" << std::endl; m_InstructionIndex++;
        m_Output << "M=D" << std::endl; m_InstructionIndex++;

        // 9. goto reg(retAddr)
        m_Output << "@" << reg(retAddr) << std::endl; m_InstructionIndex++;
        m_Output << "A=M" << std::endl; m_InstructionIndex++;
        m_Output << "0;JMP" << std::endl; m_InstructionIndex++;

#undef endFrame
#undef retAddr
    }

public:
    int Translate(int startInstructionIndex = 0)
    {
        m_CurrentTokenIndex = 0;
        m_JumpIndex = 0;
        m_InstructionIndex = startInstructionIndex;

        TranslationLoop();
        return m_InstructionIndex;
    }
};