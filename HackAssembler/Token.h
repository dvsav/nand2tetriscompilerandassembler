#pragma once

#include <string>

enum class TokenType
{
    MEMORY_ACCESS, // push | pop
    NUMBER,        // integer number
    SEGMENT,       // local | argument | this | that | constant | static | pointer | temp
    OPERATION,     // add | sub | neg | eq | gt | lt | and | or | not
    COMMENT,       // // ...
    ENDL,          // end of line
    GOTO,          // goto keyword
    IFGOTO,        // if-goto keyword
    LABEL,         // label keyword
    SYMBOL,        // some symbol (usually following 'label' or 'function' keywords)
    CALL,          // call keyword
    FUNCTION,      // function keyword
    RETURN         // return keyword
};

class Token
{
private:
    TokenType m_TokenType;
    std::string m_Value;

public:
    Token(
        TokenType tokenType,
        const std::string& value) :
        m_TokenType(tokenType),
        m_Value(value)
    { }

    TokenType getTokenType() const { return m_TokenType; }

    const std::string& getValue() const { return m_Value; }
};