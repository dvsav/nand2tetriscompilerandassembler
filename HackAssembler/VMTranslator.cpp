#include <iostream>
#include <fstream>
#include <sstream>

#if !_HAS_CXX17
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #include <filesystem>
    namespace fs = std::filesystem;
#endif

#include "LexycalAnalyzer.h"
#include "CommandTranslator.h"

int TranslateVmFile(
    std::istream& is,
    std::ostream& os,
    const std::string& className,
    int startInstructionIndex = 0);

const std::string GetAsmFileName(
    const std::string& path);

const std::string GetClassName(
    const std::string& path);

int GenerateBootstrapCode(
    std::ostream& os);

int main(int argc, char** argv)
{
    std::string path;

    if (argc == 2)
        path = argv[1];
    else
        path = fs::current_path().string();

    if (fs::is_directory(path))
    {
        fs::path dir(path);
        fs::path file(GetClassName(path));
        fs::path full_path = dir / file;
        auto asm_filename = GetAsmFileName(full_path.string());

        std::ofstream ofs(asm_filename);
        int startInstructionIndex = GenerateBootstrapCode(ofs);

        for (auto& p : fs::directory_iterator(path))
        {
            if (p.path().extension() == ".vm")
            {
                auto class_name = GetClassName(p.path().string());

                std::ifstream ifs(p.path());
                startInstructionIndex = TranslateVmFile(ifs, ofs, class_name, startInstructionIndex);
            }
        }
    }
    else
    {
        auto asm_filename = GetAsmFileName(path);
        auto class_name = GetClassName(path);

        std::ofstream ofs(asm_filename);

        int startInstructionIndex = 0;
        if(class_name == "Sys")
            startInstructionIndex = GenerateBootstrapCode(ofs);

        std::ifstream ifs(path);
        TranslateVmFile(ifs, ofs, class_name, startInstructionIndex);
    }

    return 0;
}

const std::string GetAsmFileName(
    const std::string& path)
{
    size_t last_point = path.find_last_of('.');

    std::string asm_filename = last_point != std::string::npos ?
        path.substr(0, last_point) + ".asm" :
        path + ".asm";

    return asm_filename;
}

const std::string GetClassName(
    const std::string& path)
{
    char separators[2]{ '/', '\\' };

    size_t last_separator = path.find_last_of(separators);
    size_t last_point = path.find_last_of('.');

    std::string class_name = path;
    if (last_separator != std::string::npos)
    {
        if (last_point != std::string::npos)
            class_name = path.substr(last_separator + 1, last_point - last_separator - 1);
        else
            class_name = path.substr(last_separator + 1);
    }
    else if (last_point != std::string::npos)
    {
        class_name = path.substr(0, last_point);
    }

    return class_name;
}

int TranslateVmFile(
    std::istream& is,
    std::ostream& os,
    const std::string& className,
    int startInstructionIndex)
{
    std::vector<Token> tokens = LexycalAnalyzer::Analyze(is);
    CommandTranslator ct(className, tokens, os);
    os << "// ========== " << className << " ==========" << std::endl;
    return ct.Translate(startInstructionIndex);
}

int GenerateBootstrapCode(
    std::ostream& os)
{
    int instruction_index = 0;

    // ** SP = 256 **
    os << "// " << instruction_index << std::endl;
    os << "// SP = 256" << std::endl;
    os << "@256" << std::endl; instruction_index++;
    os << "D=A" << std::endl; instruction_index++;
    os << "@SP" << std::endl; instruction_index++;
    os << "M=D" << std::endl << std::endl; instruction_index++;

    // ** LCL = 300 **
    os << "// " << instruction_index << std::endl;
    os << "// LCL = 300" << std::endl;
    os << "@300" << std::endl; instruction_index++;
    os << "D=A" << std::endl; instruction_index++;
    os << "@LCL" << std::endl; instruction_index++;
    os << "M=D" << std::endl << std::endl; instruction_index++;

    // ** ARG = 400 **
    os << "// " << instruction_index << std::endl;
    os << "// ARG = 400" << std::endl;
    os << "@400" << std::endl; instruction_index++;
    os << "D=A" << std::endl; instruction_index++;
    os << "@ARG" << std::endl; instruction_index++;
    os << "M=D" << std::endl << std::endl; instruction_index++;

    // ** THIS = 3000 **
    os << "// " << instruction_index << std::endl;
    os << "// THIS = 3000" << std::endl;
    os << "@3000" << std::endl; instruction_index++;
    os << "D=A" << std::endl; instruction_index++;
    os << "@THIS" << std::endl; instruction_index++;
    os << "M=D" << std::endl << std::endl; instruction_index++;

    // ** THAT = 3010 **
    os << "// " << instruction_index << std::endl;
    os << "// THAT = 3010" << std::endl;
    os << "@3010" << std::endl; instruction_index++;
    os << "D=A" << std::endl; instruction_index++;
    os << "@THAT" << std::endl; instruction_index++;
    os << "M=D" << std::endl << std::endl; instruction_index++;

    // ** call Sys.init 0 **
    std::istringstream iss("call Sys.init 0\n");
    return TranslateVmFile(iss, os, "bootstrap", instruction_index);
}